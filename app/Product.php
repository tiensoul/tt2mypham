<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public function type_product()
    {
        return $this->belongsTo('App/TypeProduct', 'id', 'id_type');
    }
}
