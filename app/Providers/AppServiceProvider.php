<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Session;
use App\TypeProduct;
use App\Cart;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('pages/header', function($view){
            $loaisp = TypeProduct::all();
            $view->with('loaisanpham', $loaisp);
        }); 

        view()->composer('pages/header', function($view){
            if(Session('cart')) {
                $oldCart = Session::get('cart');
                $cart = new Cart($oldCart);
                $view->with(['cart' => Session::get('cart'), 'product_cart' => $cart->items, 'totalPrice' => $cart->totalPrice, 'totalQty' => $cart->totalQty]);
            }
        });

    }
}
