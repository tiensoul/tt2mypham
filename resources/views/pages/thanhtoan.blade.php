@extends('pages/master')
@section('content')
<style>
        .alert {
            padding: 15px;
            background-color: #f44336;
            color: white;
        }
    
        .alert-success {
            padding: 15px;
            background-color: #4CAF50;
            color: white;
        }
    
        .alert-danger {
            padding: 15px;
            background-color: #f44336;
            color: white;
        }
          
          .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 15px;
            cursor: pointer;
            transition: 0.3s;
          }
          
          .closebtn:hover {
            color: black;
          }
    </style>
@php   
    $price_payment = 0;
@endphp
</div>

    <!-- MAIN CONTENT AREA -->
    <div class="container">
        <div class="row">


            <div class="site-content" role="main">

                <div class="container" style="margin: 35px 0 0 0">
                    <div class="woocommerce">
                        <div class="woocommerce-form-login-toggle">

                            <div class="woocommerce-info">
                                Bạn đã có tài khoản? <a href="{{ route('taikhoan') }}" class="showlogin">Ấn vào đây để đăng nhập</a> </div>
                        </div>
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                                <strong>Thông báo!</strong>
                            @foreach($errors->all() as $err)
                                {{ $err }}
                             @endforeach
                            </div>
                        @endif
                
                        @if(Session::has('thongbao'))
                                <div class="alert-success">
                                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
                                    <strong>Thông báo!</strong>
                                    {{ Session::get('thongbao') }}
                                </div>
                        @endif
                        @isset($products)
                        <form method="post" class="login woocommerce-form woocommerce-form-login hidden-form"
                            style="display:none;">


                            <p>Nếu trước đây bạn đã mua hàng của chúng tôi, vui lòng điền đầy đủ thông tin đăng nhập
                                dưới đây. Nếu bạn là khách hàng mới, vui lòng tiếp tục các bước tiếp theo.</p>

                            <p
                                class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide form-row-username">
                                <label for="username">Tên đang nhập hoặc email&nbsp;<span
                                        class="required">*</span></label>
                                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text"
                                    name="username" id="username" autocomplete="username" value="">
                            </p>
                            <p
                                class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide form-row-password">
                                <label for="password">Mật khẩu&nbsp;<span class="required">*</span></label>
                                <input class="woocommerce-Input woocommerce-Input--text input-text" type="password"
                                    name="password" id="password" autocomplete="current-password">
                            </p>


                            <p class="form-row">
                                <input type="hidden" id="woocommerce-login-nonce" name="woocommerce-login-nonce"
                                    value="48c72f59d6"><input type="hidden" name="_wp_http_referer" value="/thanh-toan">
                                <input type="hidden" name="redirect" value="{{ route('thanhtoan') }}">
                                <button type="submit" class="button woocommerce-Button" name="login"
                                    value="Đăng nhập">Đăng nhập</button>
                            </p>

                            

                            <span class="social-login-title">Hoặc đăng nhập bằng
                            </span>
                            <div class="basel-social-login">
                                <div class="social-login-btn">
                                    <a href="{{ route('taikhoan') }}"
                                        class="btn login-fb-link">Facebook</a>
                                </div>
                                <div class="social-login-btn">
                                    <a href="{{ route('taikhoan') }}"
                                        class="btn login-goo-link">Google</a>
                                </div>
                            </div>


                        </form>

                        

                        <form class="checkout_coupon woocommerce-form-coupon" method="post" style="display:none">

                            <p>Nếu bạn có mã giảm giá, vui lòng điền vào phía bên dưới.</p>

                            <p class="form-row form-row-first">
                                <input type="text" name="coupon_code" class="input-text" placeholder="Mã giảm giá"
                                    id="coupon_code" value="">
                            </p>

                            <p class="form-row form-row-last">
                                <button type="submit" class="button" name="apply_coupon" value="Áp dụng mã">Áp dụng
                                    mã</button>
                            </p>

                            <div class="clear"></div>
                        </form>
                        <div class="woocommerce-notices-wrapper"></div>
                        <div class="row">
                            <form name="checkout" method="post" class="checkout woocommerce-checkout"
                                action="{{ route('thanhtoan') }}" enctype="multipart/form-data"
                                novalidate="novalidate">
                                @csrf

                                <div class="col-sm-6">




                                    <div class="row" id="customer_details">
                                        <div class="col-sm-12">
                                            <div class="woocommerce-billing-fields">

                                                <h3>Thanh toán và giao hàng</h3>



                                                <div class="woocommerce-billing-fields__field-wrapper">
                                                        <p class="form-row form-row-wide address-field validate-required"
                                                        id="billing_address_1_field" data-priority="50"><label
                                                            for="billing_address_1" class="">Họ và tên&nbsp;<abbr
                                                                class="required" title="bắt buộc">*</abbr></label><span
                                                            class="woocommerce-input-wrapper"><input type="text"
                                                                class="input-text " name="name"
                                                                id="billing_address_1" placeholder="Bao gồm họ và tên" value="@if(Auth::check()) {{ Auth::user()->name }} @else {{ old('name') }} @endif"
                                                                autocomplete="address-line1"></span></p>
                                                    <p class="form-row form-row-wide address-field validate-required"
                                                        id="billing_address_1_field" data-priority="50"><label
                                                            for="billing_address_1" class="">Địa chỉ&nbsp;<abbr
                                                                class="required" title="bắt buộc">*</abbr></label><span
                                                            class="woocommerce-input-wrapper"><input type="text"
                                                                class="input-text " name="address"
                                                                id="billing_address_1" placeholder="Địa chỉ nhận hàng" value="@if(Auth::check()) {{ Auth::user()->address }} @else {{ old('address') }} @endif"
                                                                autocomplete="address-line1"></span></p>
                                                    <p class="form-row form-row-wide address-field validate-state"
                                                        id="billing_state_field" data-priority="80"><label
                                                            for="billing_state" class="">Giới tính<span
                                                                class="optional"></span></label><span
                                                            class="woocommerce-input-wrapper"><select
                                                                name="gender" id="billing_state"
                                                                class="state_select  select2-hidden-accessible"
                                                                autocomplete="address-level1" data-placeholder=""
                                                                tabindex="-1" aria-hidden="true">
                                                                <option value="">Chọn giới tính</option>
                                                                <option value="1" @if(Auth::check()) {{ Auth::user()->gender == 0 ? ' ' : 'selected' }} @else {{ old('gender') }} @endif>Nam
                                                                </option>
                                                                <option value="0" @if(Auth::check()) {{ Auth::user()->gender == 0 ? 'selected' : ' ' }} @else {{ old('gender') }} @endif>Nữ</option>
                                                            </select>
                                                            </span></p>
                                                    <p class="form-row form-row-wide validate-required validate-phone"
                                                        id="billing_phone_field" data-priority="100"><label
                                                            for="billing_phone" class="">Số điện thoại&nbsp;<abbr
                                                                class="required" title="bắt buộc">*</abbr></label><span
                                                            class="woocommerce-input-wrapper"><input type="tel"
                                                                class="input-text " name="phone"
                                                                id="billing_phone" placeholder="Số điện thoại liên lạc" value="@if(Auth::check()) {{ Auth::user()->phone }} @else {{ old('phone') }} @endif"
                                                                autocomplete="tel"></span></p>
                                                    <p class="form-row form-row-wide validate-required validate-email"
                                                        id="billing_email_field" data-priority="110"><label
                                                            for="billing_email" class="">Địa chỉ email&nbsp;<abbr
                                                                class="required" title="bắt buộc">*</abbr></label><span
                                                            class="woocommerce-input-wrapper"><input type="email"
                                                                class="input-text " name="email"
                                                                id="billing_email" placeholder="Địa chỉ email" value="@if(Auth::check()) {{ Auth::user()->email }} @else {{ old('email') }} @endif"
                                                                autocomplete="email username">
                                                                </span></p>
                                                </div>
                                            </div>

                                            <div class="woocommerce-account-fields">

                                                <p class="form-row form-row-wide create-account woocommerce-validated">
                                                    
                                                </p>




                                                <div class="create-account" style="display: none;">
                                                    <p class="form-row validate-required" id="account_password_field"
                                                        data-priority=""><label for="account_password" class="">Tạo mật
                                                            khẩu của tài khoản&nbsp;<abbr class="required"
                                                                title="bắt buộc">*</abbr></label><span
                                                            class="woocommerce-input-wrapper"><input type="password"
                                                                class="input-text " name="account_password"
                                                                id="account_password" placeholder="Mật khẩu"
                                                                value=""></span></p>
                                                    <div class="clear"></div>
                                                </div>


                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="woocommerce-shipping-fields">
                                            </div>
                                            <div class="woocommerce-additional-fields">



                                                <h3>Thông tin sản phẩm</h3>


                                                <div class="woocommerce-additional-fields__field-wrapper">
                                                    <p class="form-row notes" id="order_comments_field"
                                                        data-priority=""><label for="order_comments" class="">Ghi
                                                            chú&nbsp;<span class="optional">(tuỳ
                                                                chọn)</span></label><span
                                                            class="woocommerce-input-wrapper"><textarea
                                                                name="ghichu" class="input-text "
                                                                id="order_comments" placeholder="Lưu ý khi giao hàng"
                                                                rows="2" cols="5"></textarea></span></p>
                                                </div>


                                            </div>
                                        </div>
                                    </div>






                                </div>

                                <div class="col-sm-6">
                                    <div class="checkout-order-review  mobile-checkout-order-review card">

                                        <h3 id="order_review_heading">Đơn hàng của bạn</h3>


                                        <div id="order_review" class="woocommerce-checkout-review-order">

                                            <div class="responsive-table">
                                                <table class="shop_table woocommerce-checkout-review-order-table">
                                                    <thead>
                                                        <tr>
                                                            <th class="product-name">Sản phẩm</th>
                                                            <th class="product-total">Tổng</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $sale = 0;    
                                                        @endphp
                                                        @foreach($products as $p)
                                                        <tr class="cart_item">
                                                            <td class="product-name">
                                                                    {{ $p['item']['name'] }}&nbsp; <strong class="product-quantity">×{{ $p['qty'] }}</strong>
                                                                <!--<span class="btn-save-for-later" data-product_id="121854" data-cart-item-key="1392c6a99eeca063c4bc0de55fc51d71" style="padding: 5px 5px; font-size: 12px; color: #eb278d; display:block" >Để dành mua sau</span>-->
                                                            </td>
                                                            <td class="product-total">
                                                                <span
                                                                    class="woocommerce-Price-amount amount">
                                                                    @if($p['item']['promotion_price'] != 0)
                                                        @php
                                                            $sale += $p['qty'] * $p['item']['promotion_price'];
                                                            $price_payment += $p['qty'] * $p['item']['promotion_price'];
                                                            //echo $sale;
                                                            
                                                        @endphp
                                                            {{ $sum = number_format(( $p['qty'] * $p['item']['promotion_price']), 0, ',', '.') }}<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></span>
                                                        @else
                                                        @php
                                                            $sale += $p['qty'] * $p['item']['unit_price'];
                                                            $price_payment += $p['qty'] * $p['item']['unit_price'];
                                                            //echo $sale;
                                                            
                                                        @endphp
                                                            {{ $sum =  number_format(( $p['qty'] * $p['item']['unit_price']), 0, ',', '.') }}<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></span>
                                                        @endif
                                                                    <span
                                                                        class="woocommerce-Price-currencySymbol"></span></span>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                        @php
                                                        echo '<input type="hidden" name="price_payment" value="' . $sale . '"';    
                                                        @endphp
                                                    </tbody>
                                                    <tfoot>

                                                        <tr class="cart-subtotal">
                                                            <th>Tổng thu</th>
                                                            <td><span
                                                                    class="woocommerce-Price-amount amount">@php
                                                                    echo number_format(($sale), 0, ',', '.');
                                                              @endphp<span
                                                                        class="woocommerce-Price-currencySymbol">₫</span></span>
                                                            </td>
                                                        </tr>




                                                        <tr class="woocommerce-shipping-totals shipping">
                                                            <th>Giao hàng</th>
                                                            <td data-title="Giao hàng">
                                                                Giao hàng tiêu chuẩn

                                                            </td>
                                                        </tr>






                                                        <tr class="order-total">
                                                            <th>Tổng</th>
                                                            <td><strong><span
                                                                        class="woocommerce-Price-amount amount">@php
                                                                        echo number_format(($sale), 0, ',', '.');
                                                                  @endphp<span
                                                                            class="woocommerce-Price-currencySymbol">₫</span></span></strong>
                                                            </td>
                                                        </tr>


                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div id="payment" class="woocommerce-checkout-payment">
                                                <ul class="wc_payment_methods payment_methods methods">
                                                    <li class="wc_payment_method payment_method_bacs">
                                                        <input id="payment_method_bacs" type="radio" class="input-radio"
                                                            name="payment_method" value="COD" checked="checked"
                                                            data-order_button_text="">

                                                        <label for="payment_method_bacs">
                                                            Trả tiền mặt khi nhận hàng (COD) </label>
                                                        <div class="payment_box payment_method_bacs"
                                                            style="display: block;">
                                                            <p>Quý khách hàng vui lòng thanh toán tiền mặt cho nhân viên giao hàng khi nhận sản phẩm.</p>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="form-row place-order">
                                                    <noscript>
                                                        Since your browser does not support JavaScript, or it is
                                                        disabled, please ensure you click the &lt;em&gt;Update
                                                        Totals&lt;/em&gt; button before placing your order. You may be
                                                        charged more than the amount stated above if you fail to do so.
                                                        <br /><button type="submit" class="button alt"
                                                            name="woocommerce_checkout_update_totals"
                                                            value="Cập nhật tổng">Cập nhật tổng</button>
                                                    </noscript>

                                                    <div class="woocommerce-terms-and-conditions-wrapper">
                                                        <div class="woocommerce-privacy-policy-text"></div>
                                                    </div>


                                                    <button type="submit" class="button alt"
                                                        name="woocommerce_checkout_place_order" id="place_order"
                                                        value="Đặt hàng">Đặt hàng</button>

                                                    
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>

                            </form>

                        </div>
                        @endisset


                    </div>
                    <div class="row">
                    </div>
                </div>


            </div>
            <style type="text/css">
                .scrollToTop {
                    display: none;
                }
            </style>
        </div> <!-- end row -->
    </div> <!-- end container -->
</div>
@endsection('content')