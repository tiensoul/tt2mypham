<!-- FOOTER -->
		<!---DESKTOP-->
		<footer class="footer-container hidden-xs">

                <div class="top-footer-wrap">
    
                    <div class="gtmh-nuty-section">
                        <div class="container">
                            <div class="row">
    
    
                            </div>
                        </div>
                    </div>
    
                    <div class="container">
                        <div class="row">
                        </div>
                    </div>
                </div>
    
                <div class="copyrights-wrapper copyrights-centered">
                    <div class="container">
                            <div class="column">
                                    <div class="basel-logo-wrap switch-logo-enable">
                                            <a href="index.html" class="basel-logo basel-main-logo" rel="home">
                                                <img width="200" height="104" src="wp-content/uploads/2016/05/logo.png" alt="Nuty Cosmetics">
                                            </a>
                                    </div>
                                </div>
                                <div class="column">
                                    <h4>LIÊN HỆ</h4>
                                    <p>Địa chỉ: số 31 Phan Đình Giót, Phường Phương Liệt, Thanh Xuân, Hà Nội</p>
                                    <p>Phone: 1800.xxx.xxx</p>
                                </div>
                                <div class="column">
                                    <h4>HƯỚNG DẪN</h4>
                                    <a href="{{ route('taikhoan') }}">Đăng ký tài khoản</a>
                                    <a href="{{ route('loaisp', 1) }}">Phụ kiện trang điểm</a>
                                    <a href="{{ route('loaisp', 2) }}">Sản phẩm chăm sóc da</a>
                                    <a href="{{ route('loaisp', 5) }}">Nước hoa nhập khẩu</a>
                                </div>				
                </div>
    
            </footer>
            <!---END DESKTOP-->